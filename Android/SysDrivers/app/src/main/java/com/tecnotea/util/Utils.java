package com.tecnotea.util;

import java.text.SimpleDateFormat;


public class Utils {

	/**
	 * Formatta una data, il valore mostrato dipende dal 'locale'.
	 * Attenzione:
	 *   la 'SimpleDateFormat()' è molto lenta non usarla in loop !!
	 *
	 */
	public static String timestampToLocal(String pattern, long timestamp) {
		try {
			return (new SimpleDateFormat(pattern)).format(timestamp);
		}
		catch (Exception ex) {
			Logger.exception("Utils.timestampToLocal()", ex);
			return "";
		}
	}


	/**
	 * Formatta una data corrente, il valore mostrato dipende dal 'locale'
	 *
	 */
	public static String timestampToLocal(String pattern) {
		return timestampToLocal(pattern, System.currentTimeMillis());
	}

}