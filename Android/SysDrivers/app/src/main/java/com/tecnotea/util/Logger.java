package com.tecnotea.util;

import android.os.Environment;
import android.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;

import com.tecnotea.sysdrivers.Globals;



/**
 *  Logga in:
 *    cat /storage/emulated/0/Android/data/com.tecnotea.sysdrivers/logs/info.txt
 *
 * Pe scaricare
 *    adb pull -a /storage/emulated/0/Android/data/com.tecnotea.sysdrivers/logs/info.txt d:\tmp\info.txt
 *
 */
public class Logger {

	private static final String FILE_INFO = "/info.txt";
	private static final String LOGS_FOLDER  = Globals.PUBLIC_APP_FOLDER + "/logs";


	/**
	 * Esegue una scrittura generica su un file specificato
	 *
	 */
	private static void write(String filename, String where, String sText)  {

		// crea dir se non esiste
		File oFile = new File(Environment.getExternalStorageDirectory() + LOGS_FOLDER);
		if (oFile.isDirectory() == false) {
			oFile.mkdirs();
		}

		// oggetto file
		oFile = new File(Environment.getExternalStorageDirectory() + LOGS_FOLDER + "/" + filename);

		// cancella il file se piu' vecchio di 12 ore
		if (((new Date()).getTime() - (new Date(oFile.lastModified())).getTime()) > (12 * 3600 * 1000)) {
			oFile.delete();
		}

		try {
			FileOutputStream fOut = new FileOutputStream(oFile, true);
			fOut.write(("[" + Utils.timestampToLocal("HH:mm:ss.SSS") +  "] - " + where + ": " + sText + "\r\n").getBytes());
			fOut.close();
	    }
	  	catch (Exception ex) {
	  		ex.printStackTrace();
	    }
	}


	/**
	 * Esegue un log di info
	 *
	 */
	public static void info(String where, String sText)  {
		write(FILE_INFO, where, sText);
		Log.d(Globals.TAG, where + " - " + sText);
	}


	/**
	 * Esegue un log di errore, sia su file che su logcat
	 *
	 */
	public static void error(String where, String sText)  {
		write(FILE_INFO, where, sText);
		Log.d(Globals.TAG, where + " - " + sText);
	}


	/**
	 * Esegue un log di una exception
	 *
	 */
	public static void exception(String where, Exception ex)  {
		ex.printStackTrace();
		StringWriter errors = new StringWriter();
		ex.printStackTrace(new PrintWriter(errors));
		Logger.info(where, errors.toString());
	}
}
