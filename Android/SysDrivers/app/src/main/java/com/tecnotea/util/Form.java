package com.tecnotea.util;

import android.content.Context;
import android.widget.Toast;


public class Form
{

	/**
	 *  Show alert message box, uso:
	 *  	showToastResource.alert(ActivityMain.this, R.mValue.system_error);
	 */
	public static void showToast(Context context, int iMessage) {
		Form.showToast(context, context.getResources().getString(iMessage));
	}


	/**
	 *  Show alert message box, uso:
	 *  	showToastResource.alert(ActivityMain.this, "Ciao!");
	 */
	public static void showToast(Context context, String sMessage) {
		Toast.makeText(context, sMessage, Toast.LENGTH_LONG).show();
	}

}