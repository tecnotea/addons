package com.tecnotea.sysdrivers;

import android.app.Activity;
import android.app.AlarmManager;
import android.content.Context;
import android.os.Bundle;

import com.tecnotea.util.Form;
import com.tecnotea.util.Logger;


/**
 * Android Things activity.
 *
 */
public class ActivityMain extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // parametri action
        String action = getIntent().getAction();
        long millis = getIntent().getLongExtra("millis", -1);

        // logga !
        Logger.info("ActivityMain.onCreate", "action: " + action + ", millis: " +  Long.toString(millis));

        // testa che fare
        if ((action.equals("com.tecnotea.sysdrivers.SET_TIME")) && (millis != -1)) {

            // setta l'ors di sistema
            try {
                ((AlarmManager) getSystemService(Context.ALARM_SERVICE)).setTime(millis);
            }
            catch (Exception ex) {
                Logger.exception("ActivityMain.onCreate", ex);
                Form.showToast(this, ex.getMessage());
            }
        }

        // chiudi
        finish();
    }
}
