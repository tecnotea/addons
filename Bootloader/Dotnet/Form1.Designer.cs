﻿namespace At_Updator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.textBoxStatusMessage = new System.Windows.Forms.TextBox();
            this.buttonRunProgram = new System.Windows.Forms.Button();
            this.buttonUpdateProgram = new System.Windows.Forms.Button();
            this.buttonTestConnection = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxStatusMessage
            // 
            this.textBoxStatusMessage.Enabled = false;
            this.textBoxStatusMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxStatusMessage.Location = new System.Drawing.Point(23, 139);
            this.textBoxStatusMessage.Name = "textBoxStatusMessage";
            this.textBoxStatusMessage.Size = new System.Drawing.Size(416, 24);
            this.textBoxStatusMessage.TabIndex = 27;
            // 
            // buttonRunProgram
            // 
            this.buttonRunProgram.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRunProgram.Location = new System.Drawing.Point(245, 73);
            this.buttonRunProgram.Name = "buttonRunProgram";
            this.buttonRunProgram.Size = new System.Drawing.Size(194, 32);
            this.buttonRunProgram.TabIndex = 28;
            this.buttonRunProgram.Text = "Run program";
            this.buttonRunProgram.UseVisualStyleBackColor = true;
            this.buttonRunProgram.Click += new System.EventHandler(this.ButtonRunProgram_Click);
            // 
            // buttonUpdateProgram
            // 
            this.buttonUpdateProgram.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUpdateProgram.Location = new System.Drawing.Point(23, 73);
            this.buttonUpdateProgram.Name = "buttonUpdateProgram";
            this.buttonUpdateProgram.Size = new System.Drawing.Size(194, 32);
            this.buttonUpdateProgram.TabIndex = 29;
            this.buttonUpdateProgram.Text = "Update program";
            this.buttonUpdateProgram.UseVisualStyleBackColor = true;
            this.buttonUpdateProgram.Click += new System.EventHandler(this.ButtonUpdateProgram_Click);
            // 
            // buttonTestConnection
            // 
            this.buttonTestConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonTestConnection.Location = new System.Drawing.Point(23, 20);
            this.buttonTestConnection.Name = "buttonTestConnection";
            this.buttonTestConnection.Size = new System.Drawing.Size(194, 32);
            this.buttonTestConnection.TabIndex = 30;
            this.buttonTestConnection.Text = "Test connection";
            this.buttonTestConnection.UseVisualStyleBackColor = true;
            this.buttonTestConnection.Click += new System.EventHandler(this.ButtonTestConnection_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(473, 185);
            this.Controls.Add(this.buttonTestConnection);
            this.Controls.Add(this.buttonUpdateProgram);
            this.Controls.Add(this.buttonRunProgram);
            this.Controls.Add(this.textBoxStatusMessage);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AT-Updator";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxStatusMessage;
        private System.Windows.Forms.Button buttonRunProgram;
        private System.Windows.Forms.Button buttonUpdateProgram;
        private System.Windows.Forms.Button buttonTestConnection;
    }
}

