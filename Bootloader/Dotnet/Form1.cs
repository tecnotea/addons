﻿using System;
using System.Configuration;
using System.IO;
using System.IO.Ports;
using System.Threading;
using System.Timers;
using System.Windows.Forms;


namespace At_Updator
{
    public partial class Form1 : Form
    {
        // comandi al firmware
        private const int CMD_GOTO_BOOTLOADER = 0x51;

        // comandi protocollo
        private const int CMD_TEST_BOOTLOADER = 0x60;
        private const int CMD_PROG_BOOTLOADER = 0x61;
        private const int CMD_END_BOOTLOADER  = 0x62;

        private Thread mReceiveThread = null;
        private Serial mSerialObj = new Serial();
        private int mLineIndex = 0;
        private string[] mHexFile = null;
        private System.Timers.Timer mTimerLine = null;



        /**
         * Formatta tramite un delgate un valore stringa
         * 
         */
        private void GenericFormat(System.Windows.Forms.TextBox textBox, String value)
        {
            textBox.Invoke(new MethodInvoker(delegate { textBox.Text = value; }));
        }
        

        /**
         * Esegui un test
         * 
         */
        private void ButtonTestConnection_Click(object sender, EventArgs e)
        {
            buttonTestConnection.Enabled = false;
            GenericFormat(textBoxStatusMessage, "Waiting...");
            mTimerLine.Start();
            mSerialObj.Send(CMD_TEST_BOOTLOADER);
        }


        /**
         * Lancia la neo-isola
         * 
         */
        private void ButtonRunProgram_Click(object sender, EventArgs e)
        {
            mSerialObj.Send(CMD_END_BOOTLOADER);
            GenericFormat(textBoxStatusMessage, "Sent started command.");
        }


        /**
         * Lancia l'update
         * 
         */
        private void ButtonUpdateProgram_Click(object sender, EventArgs e)
        {
            // displays an OpenFileDialog
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Binary executable files|*.hex";
            openFileDialog.Title = "Select a file";

            // show the Dialog e leggi il file da caricare  
            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK) {

                try  {
                    mHexFile = File.ReadAllLines(openFileDialog.FileName);
                }
                catch (IOException ex) {
                    GenericFormat(textBoxStatusMessage, "Open file error: " + ex.Message);
                    mHexFile = null;
                    return;
                }
            }

            // fa partire il programma di bootloader sul firmware
            mSerialObj.Send(CMD_GOTO_BOOTLOADER);

            // attendi il bootloader partito e invia la richiesta al bootloader
            Thread.Sleep(100);
            
            // richiede l'update con timeout
            mTimerLine.Start();
            GenericFormat(textBoxStatusMessage, "Starting updating...");

            // invia prima riga del file hex
            mLineIndex = 0;
            mSerialObj.Send(CMD_PROG_BOOTLOADER, mHexFile[mLineIndex++]);
        }


        /**
         * Gestisce i timeout di linea
         * 
         */
        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            mTimerLine.Stop();
            GenericFormat(textBoxStatusMessage, "Error, line timeout.");
            Invoke(new MethodInvoker(delegate { buttonTestConnection.Enabled = true; }));
        }


        /**
         * Thread ricezione dati
         * 
         */
        private void ReceiveThread() {

            // loop infinito o quasi !!
            while (mReceiveThread != null)  {

                // attendi un po
                RxData data = mSerialObj.Receive();
                if (data == null) {
                    continue;
                }

                // testa se errore
                if (data.error != null)  {
                    GenericFormat(textBoxStatusMessage, data.error);
                    continue;
                }

                // ok
                byte[] buff = data.data;

                // comando
                switch (buff[0]) {

                    case CMD_TEST_BOOTLOADER :
                        mTimerLine.Stop();
                        GenericFormat(textBoxStatusMessage, "Connection test OK.");
                        Invoke(new MethodInvoker(delegate { buttonTestConnection.Enabled = true; }));
                        break;


                    case CMD_PROG_BOOTLOADER:
                        mTimerLine.Stop();
                        mTimerLine.Start();
                        mSerialObj.Send(CMD_PROG_BOOTLOADER, mHexFile[mLineIndex++]);
                        GenericFormat(textBoxStatusMessage, "Update in progress, rows: " + mLineIndex);
                        break;


                    case CMD_END_BOOTLOADER :
                        mTimerLine.Stop();
                        mSerialObj.Send(CMD_END_BOOTLOADER);
                        break;
                        

                    default:
                        GenericFormat(textBoxStatusMessage, "Invalid comand");
                        break;
                }
            }
        }


        /**
         * Inizializza l'app
         * 
         */
        public Form1() {
            InitializeComponent();
        }
        

        /**
         * Inizializza il tutto
         * 
         */
        private void Form1_Load(object sender, EventArgs e) {

            int speed = 115200;
            String com = "COM8";
           

            // prova a leggegere app.config
            try {

                // leggi config
                string s = ConfigurationManager.AppSettings.Get("baudrate");
                speed = (s.Length > 0) ? Convert.ToInt32(s) : 115200;
                com = ConfigurationManager.AppSettings.Get("com");

                // setta il titolo
                Text += "  -  " + com + "  " + speed.ToString() + " bps";
            }
            catch (Exception) {
                MessageBox.Show("Configuration not found in app.config. Use default configuration values.");
            }

            // inizializza rs232
            string ret = mSerialObj.Inizialize(com, speed);

            // esito inizializzazione
            if (ret != null) {
                MessageBox.Show("Initialization failed. " + ret + ". The application can't continue.");
                Application.Exit();
            }

            // crea timer per il timeout di linea
            mTimerLine = new System.Timers.Timer(2000);
            mTimerLine.Elapsed += new ElapsedEventHandler(TimerElapsed);
            mTimerLine.Stop();

            // lancia il thread
            mReceiveThread = new Thread(new ThreadStart(this.ReceiveThread));
            mReceiveThread.IsBackground = true;
            mReceiveThread.Start();
        }


        /**
         * Chiusura
         * 
         */
        private void Form1_FormClosed(object sender, FormClosedEventArgs e) {
            mReceiveThread = null;
            mTimerLine.Stop();
            mSerialObj.Terminate();
        }
    }
}
