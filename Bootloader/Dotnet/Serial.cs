﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Text;
using System.Threading;


namespace At_Updator
{

    class RxData
    {
        public RxData(byte[] data, String error) {
            this.data = data;
            this.error = error;
        }

        public byte[] data = null;
        public String error = null;
    }


    class Serial
    {
        private const byte STX = 0x02;
        private const byte ETX = 0x03;
        private const byte DLE = 0x10;       // data link escape
        private const byte OFS = 0x20;

        SerialPort mSerialPort = null;
        Boolean mDleStatus = false;
        List<byte> mProtBuff = null;


        /**
         * Inizializza il dispositivo
         * 
         */
        public string Inizialize(String com, int baud)
        {
            mSerialPort = new SerialPort();

            mSerialPort.PortName = com;
            mSerialPort.BaudRate = baud;
            mSerialPort.Parity = Parity.None;
            mSerialPort.DataBits = 8;
            mSerialPort.StopBits = StopBits.One;
            mSerialPort.Handshake = Handshake.None;
            mSerialPort.ReadTimeout = SerialPort.InfiniteTimeout;
            mSerialPort.WriteTimeout = SerialPort.InfiniteTimeout;
            mSerialPort.Open();

            // protocollo
            mDleStatus = false;
            mProtBuff = null;

            // ok
            return null;
        }


        /**
         * Torna lo stato
         * 
         */
        public string GetStatus()
        {
            return null;
        }


        /**
         * Ricezione dati asincrona
         * 
         */
        public RxData Receive()
        {
            byte ch;

            if (mSerialPort == null)
            {
                return new RxData(null, "Port closed");
            }

            // prova a leggere
            try
            {
                // -1 fine flusso
                int val = mSerialPort.ReadByte();
                if (val == -1) {
                    return new RxData(null, "End of flow");
                }

                // ok
                ch = (byte)val;
            }
            catch (Exception ex)
            {
                return new RxData(null, ex.Message);
            }

            // testa carattere
            switch (ch)
            {
                case STX:
                    mDleStatus = false;
                    mProtBuff = new List<byte>();
                    break;

                case ETX:
                    if (mProtBuff != null)
                    {
                        // per sicurezza
                        mDleStatus = false;

                        // testa condizione STX, ETX
                        if (mProtBuff.Count == 0)
                        {
                            mProtBuff = null;
                            return new RxData(null, "Empty buffer");
                        }

                        // buffer di ritorno, senza il crc
                        byte crc = 0;
                        byte[] ret = new byte[mProtBuff.Count - 1];

                        // copia dati e calcola crc
                        for (int i = 0; i < mProtBuff.Count - 1; i++)
                        {
                            ret[i] = mProtBuff[i];
                            crc += mProtBuff[i];
                        }

                        // se errore crc non torna nulla
                        if (mProtBuff[mProtBuff.Count - 1] != crc)
                        {
                            mProtBuff = null;
                            return new RxData(null, "CRC error");
                        }

                        // dati letti
                        mProtBuff = null;

                        // ok !
                        return new RxData(ret, null);
                    }
                    break;

                case DLE:
                    mDleStatus = true;
                    break;

                default:

                    // il primo dato deve essere un STX
                    if (mProtBuff == null) {
                        return new RxData(null, "Sequence error");
                    }

                    // tasta se in stato escape
                    if (mDleStatus)
                    {
                        mDleStatus = false;
                        ch = (byte)(ch - OFS);
                    }

                    // salva il dato
                    mProtBuff.Add(ch);
                    break;
            }
            
            // dato non acora costruito
            return null;
        }


        /**
         * Invia un comando di tipo byte
         * 
         */
        public string Send(byte command)
        {
            byte[] tx = { command };
            return Send(tx);
        }


        /**
         * Invia un comando più una stringa
         * 
         */
        public string Send(byte command, String data)
        {            
            byte[] txbuff = new byte[data.Length + 1];            
            txbuff[0] = command;

            for (int i = 0; i < data.Length; i++)
            {
                txbuff[i + 1] = (byte)data[i];
            }             

            return Send(txbuff);
        }


        /**
         * Invia un buffer 
         * 
         */
        public string Send(byte[] txParam)
        {
            byte crc = 0;
            List<byte> txbuff = new List<byte>();

            // testa porta aperta
            if (mSerialPort == null)
            {
                return "Error: port close";
            }

            // calcola e appende il crc
            for (int i = 0; i < txParam.Length; i++)
            {
                crc += txParam[i];
                txbuff.Add(txParam[i]);
            }
            txbuff.Add(crc);

            // buffer da inviare con byte stuffing
            List<byte> bout = new List<byte>();
            bout.Add(STX);

            // copia
            for (int i = 0; i < txbuff.Count; i++)
            {
                byte ch = txbuff[i];
                if (ch == STX)
                {
                    bout.Add(DLE);
                    bout.Add((byte)(ch + OFS));
                }
                else if (ch == ETX)
                {
                    bout.Add(DLE);
                    bout.Add((byte)(ch + OFS));
                }
                else if (ch == DLE)
                {
                    bout.Add(DLE);
                    bout.Add((byte)(ch + OFS));
                }
                else {
                    bout.Add(ch);
                }
            }

            // fine buffer
            bout.Add(ETX);

            // ok, invio
            mSerialPort.Write(bout.ToArray(), 0, bout.Count);
            return null;
        }


        /**
         * Chiude tutto !
         * 
         */
        public void Terminate()
        {
            mSerialPort.Close();
            mSerialPort = null;
        }
    }
}
