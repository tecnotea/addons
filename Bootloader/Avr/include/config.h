//******************************************************************************
//! @file $RCSfile: config.h,v $
//!
//! Copyright (c) 2007 Atmel.
//!
//! Use of this program is subject to Atmel's End User License Agreement.
//! Please read file license.txt for copyright notice.
//!
//! @brief Configuration file for the following project:
//!             - can_song_server_example_gcc
//!
//! This file can be parsed by Doxygen for automatic documentation generation.
//! This file has been validated with AVRStudio-413528/WinAVR-20070122.
//!
//! @version $Revision: 3.20 $ $Name: jtellier $
//!
//! @todo
//! @bug
//******************************************************************************

#ifndef _CONFIG_H_
#define _CONFIG_H_

#include "compiler.h"
#include <avr/io.h>
#include <avr/interrupt.h>


// Bootloader definition
#define BOOT_LOADER_SIZE        0x2000  // Size in bytes: 8KB
#define MAX_FLASH_SIZE_TO_ERASE (FLASH_SIZE - ((U32)(BOOT_LOADER_SIZE)))

// preocessor definition
#define MANUF_ID                 0x1E                        // ATMEL
#define FAMILY_CODE              0x81                        // AT90CANxxx family
#define XRAM_END                 XRAMEND                     // Defined in "iocanxx.h"
#define RAM_END                  RAMEND                      // Defined in "iocanxx.h"
#define E2_END                   E2END                       // Defined in "iocanxx.h"
#define FLASH_END                FLASHEND                    // Defined in bytes in "iocanxx.h"
#define FLASH_SIZE               ((U32)(FLASH_END)) + 1      // Size in bytes
#define FLASH_PAGE_SIZE          256                         // Size in bytes, constant for AT90CANxx devices

// UART lib configuration
#define USE_TIMER16              TIMER16_1
#define USE_UART                 BOTH_UART
#define UART_BAUDRATE            115200

// MCU lib configuration
#define FOSC                     12000                       // 8 MHz External cristal
#define F_CPU                    (FOSC * 1000)               // Need for AVR GCC
#define CAN_BAUDRATE             125                         // in kBit (o CAN_AUTOBAUD)

#endif  // _CONFIG_H_
