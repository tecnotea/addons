/*
 * main.c
 *
 * Created: 07-Jul-18 5:48:01 PM
 * Author : AS
 *
 * Settare fuse, in: Tools / Device Programming (fare apply) / Fuses
 *
 *    HIGH.BOOTSZ  Boot flash size = 4096 word, Start address = 0xF000
 *    Attenzione � indirizzamento a word equivale in indirizzamento a byte al valore 0x1E000
 *
 *    HIGH.BOOTRST abilitato
 * 
 * In Properties / Toolchain / AVR/GNU Linker / Memory Settings / Flash segment
 *
 *    .text=0xF000
 *
 *    Attenzione, anche questo � inteso come indirizzamento a word.
 *
 *    Il bootloader rimane in attesa per circa 2 secondi di un comando che pu� arrivare o dalla porta
 *    RS232 o dal CAN-BUS. Se tale comando non arriva viene eseguita l'istruzione all'indirizzo zero.
 *    Se � presente solo il bootloader, ed all'indirizzo zero non c'e' nulla, il bootloader torna di
 *    nuovo in attesa.
 *
 *    I vari firmware quando installati, per poter essere aggiornati dal bootloader, devono riconoscere
 *    un comando che scateni il bootloader, (in teoria basterebbe un reset hardware).
 * 
 *
 */ 

#include <stdlib.h>
#include <avr/boot.h>
#include "atmel/can_lib.h"
#include "atmel/uart_lib.h"


// mie macro (usano extended: >16 bit address)
#define myPageErase(addr)   { boot_page_erase(addr); boot_spm_busy_wait(); }
#define myPageWrite(addr)   { boot_page_write(addr); while(boot_rww_busy()) boot_rww_enable(); }


// dimensione massima buffer
#define DIM_RXBUFFER                128

// controlli per il protocollo su seriale
#define STX 0x02
#define ETX 0x03
#define DLE 0x10									// data link escape
#define OFS 0x20

// stati per il protocollo seriale
#define STATUS_RS232_STX	        0
#define STATUS_RS232_DATA	        1
#define STATUS_RS232_DLE            2

// comandi protocollo  seriale
#define CMD_TEST_BOOTLOADER         0x60
#define CMD_PROG_BOOTLOADER         0x61
#define CMD_END_BOOTLOADER          0x62

// can id
#define ID_CAN_MONITOR              100
#define ID_CAN_PROG_BOOTLOADER      11
#define ID_CAN_END_BOOTLOADER       12

// risposte su can (attenzione coerenza con app)
#define ID_VALUE_PROG_BOOTLOADER    10
#define ID_VALUE_END_BOOTLOADER     11
#define ID_VALUE_RESEND_BOOTLOADER  12

// stati ricezione can-bus
#define STATUS_CAN_ACCEPTED         0
#define STATUS_CAN_COMPLETED        1

// dimensione di una pagina in word
#define PAGESIZE_INWORDS            (SPM_PAGESIZE/2)

// ritorno dalla updateProgram
#define RESULt_FINISHED             1
#define RESULt_CONTINUE             2
#define RESULt_FAILED               3


// variabili per la programmazione
static U32 mSegAddr = 0;	
static U32 mOldAddr = 0;	
static int mCntWord = 0;	

// dati  protocollo rs232
static U8 mProtStatus = STATUS_RS232_STX;
	
// dati ricezione can-bus
static st_cmd_t mMsg = { 0 };
static U8 mCanBuffer[8] = { 0 };
static U8 mRecvStatus = STATUS_CAN_ACCEPTED;

// buffer per ricezione dati
static U8 mIndexBuffer = 0;
static U8 mLineBuffer[DIM_RXBUFFER] = { 0 }; 	


/**
 * Inizializza seriale
 *
 */
static void uartinit(void) {
	uart_selected = UART_0;
	Uart_clear();
	Uart_set_baudrate(UART_BAUDRATE);
	Uart_hw_init(CONF_8BIT_NOPAR_1STOP);
	Uart_enable();
}


/**
 * Converti una stringa in un binario a 8 bit
 *
 */
static U8 str2byte(U8 *b) {	
	U8 clo = *(b + 1);
	U8 chi = *(b + 0);
	U8 lo = ((clo >= 0x30) && (clo <= 0x39)) ? clo - 0x30 : clo - 0x37;
	U8 hi = ((chi >= 0x30) && (chi <= 0x39)) ? chi - 0x30 : chi - 0x37;	
	return (hi << 4) | lo;
}


/**
 * Converti una stringa in un binario a 16 bit
 *
 */
static U16 str2word(U8 *b) {		
	return (((U16)str2byte(b)) << 8) | (U16)str2byte(b + 2);  		
}


/**
 * Scrivi sulla flash, leggendo un baffer di questo tipo (senza spazi):
 *  
 *   :10   E440   00    49F780916E01882329F486EC90E0FC01  65
 *    len  addr   tipo  dati                              check
 * 
 */
static int updateLineFirmware(U8 *bufferLine) {	

	U8 buff[16] = { 0 };

	// testata
	U8 reclen = str2byte(bufferLine + 1); 
	U32 addrress = (U32)str2word(bufferLine + 3);  	
	U8 rectype = str2byte(bufferLine + 7); 

	// dati
	for (int i = 0; i < reclen; i++) {
		U8 val = str2byte(bufferLine + 9 + (i * 2)); 	
		buff[i] = val;
	}

	// leggi checsum
	U8 checkval = str2byte(bufferLine + 9 + (reclen * 2));

	// calcola il check su tutt la riga
	U8 checkcmp = 0;
	for (int i = 0; i < reclen + 4; i++) {
		checkcmp += str2byte(bufferLine + 1 + (i * 2));
	}

	// confronta check in complemento a 2, se errore torna KO
	if (checkval != (U8)((-(U16)checkcmp) & 0x00FF)) {
		return RESULt_FAILED;
	}

	// conclusione se fine record (0x01)
	if (rectype == 1) {
		
		// testa dati residui da salvare
		if (mCntWord >= 0) {
			mCntWord = 0;			
			myPageWrite(mOldAddr);
		}

		// OK, fine
		return RESULt_FINISHED;
	}

	// testa se 'Segment Address' e ricava indirizzo segmento
	if (rectype == 2) {	
		mSegAddr = ((U32)buff[0] << 12) | ((U32)buff[1] << 4);
	}

	// scrivi su flash
	else if (rectype == 0) {	
	
		// scrivi a word
		for (U8 i = 0; i < reclen; i += 2) {

			// se inzio pagina, la cancella
			if (mCntWord == 0) {
				mOldAddr = mSegAddr + addrress;
				myPageErase(mOldAddr);
			}
		
			// scrivi una word
			U16 wdata = (U16)buff[i] | (((U16)buff[i+1]) << 8);
			boot_page_fill(mSegAddr + addrress + i, wdata);								// extended: >16 bit address
		
			// se scritta intera pagina scrivi sulla flash e attende
			if (++mCntWord >= PAGESIZE_INWORDS) {
				mCntWord = 0;
				myPageWrite(mOldAddr);
			}
		}
	}
	
	// ok
	return RESULt_CONTINUE;
}


/**
 * Metodo sincrono per inviate dati sul can-bus.
 * 
 */
static void canSend(U8 reply) {
	
	st_cmd_t msg;	
	U8 txbuff[8] = { 0 };

	// un solo byte di risposta
	txbuff[0] = reply;
	
	// salva dati invio	
	msg.dlc = sizeof(txbuff);
	msg.pt_data = txbuff;
	msg.id.std = ID_CAN_MONITOR;
	msg.cmd = CMD_TX_DATA;	
	msg.ctrl.ide = 0;								// can 2.0A

	// send command
	while(can_cmd(&msg) != CAN_CMD_ACCEPTED);
	while(can_get_status(&msg) == CAN_STATUS_NOT_COMPLETED);

	// small tempo: inteframe = 256us instead of 96us
	for(int i = 0; i < 0xFF; i++) {
		GPIOR0=i;
	}	
}


/**
 * Metodo sincrono per inviate dati sulla seriale
 *
 */
static void rs232Send(U8 cmd) {
	
	// non ha dati, non calcola stuffing per semplicita'
	U8 txbuff[] = { STX, cmd, 0, 0, 0, cmd, ETX };

	// invio buffer	
	for(int i = 0; i < sizeof(txbuff); i++) {
		uart_putchar(txbuff[i]);
	}
}


/**
 *  Protocollo per la seriale
 *
 */
static Bool rs232Protocol(U8 ch) {
	
	// stato protocollo
	switch (mProtStatus)  {
			
		case STATUS_RS232_STX :
			if (ch == STX) {
				mIndexBuffer = 0;
				mProtStatus = STATUS_RS232_DATA;
			}
			break;

		case STATUS_RS232_DATA :

			// se di nuovo STX re-resetta indice buffer
			if (ch == STX) {
				mIndexBuffer = 0;
			}
			
			// testa se escape
			else if (ch == DLE) {
				mProtStatus = STATUS_RS232_DLE;
			}
			
			// testa fine pacchetto
			else if (ch == ETX) {

				// prossimo stato
				mProtStatus = STATUS_RS232_STX;
				
				// fine pacchetto, testa se buffer vuoto (almeno il CRC)
				if (mIndexBuffer == 0) {
					break;
				}

				// ricava CRC
				U8 crc = 0;
				for (int i = 0; i < mIndexBuffer - 1; i++) {
					crc += mLineBuffer[i];
				}
				
				// se crc ok, buffer completo
				if (mLineBuffer[mIndexBuffer - 1] == crc) {
					return TRUE;
				}
			}
			else {
				
				// default, salva il dato
				mLineBuffer[mIndexBuffer++] = ch;
				
				//  dimensione massima
				if (mIndexBuffer >= DIM_RXBUFFER) {
					mProtStatus = STATUS_RS232_STX;
				}
			}
			break;

		case STATUS_RS232_DLE :
			
			// salva dato con stuffing
			mLineBuffer[mIndexBuffer++] = ch - OFS;
			
			// testa posizione su buffer
			if (mIndexBuffer >= DIM_RXBUFFER) {
				mProtStatus = STATUS_RS232_STX;
			}
			else {
				mProtStatus = STATUS_RS232_DATA;
			}
			break;
			
		default :			
			break;
	}
	
	// ok
	return FALSE;
}


/**
 * Ingresso pricipale per il bootloader
 *
 */
 int main(void) {

	// porta C ed E in uscita con livelli alti
	DDRC = 0xFF;
	DDRE = 0xFF;
	PORTE = 0xFF;

	// inizializza UART e cab-bus
	uartinit();
	can_init(0);
	
	// inizializza programmzione
	mCntWord = 0;
	mOldAddr = 0;	
	mSegAddr = 0;

	// inizializza stati procollo rs232
	mProtStatus = STATUS_RS232_STX;

	// inizializza stati procollo can-bus
	mMsg.pt_data = mCanBuffer;
	mMsg.dlc = sizeof(mCanBuffer);
	mMsg.cmd = CMD_RX_DATA;
	mMsg.ctrl.ide = 0;
	mRecvStatus = STATUS_CAN_ACCEPTED;

	// indice usato per entrambi i protocolli
	mIndexBuffer = 0;


	// loop  gestione protocollo con timeout assenza dati (2 sec)
    for (long cnt = 0; cnt < 200000; cnt++)  {

		// gestione rs232,  testa se dato presente sulla seriale
		if (Uart_rx_ready() != 0) {

			// leggi il carattere ricevuto
			U8 ch = Uart_get_byte();
			Uart_ack_rx_byte();

			// gestione protocollo
			if (rs232Protocol(ch)) {
			
				// conando di test
				if (mLineBuffer[0] == CMD_TEST_BOOTLOADER) {
					rs232Send(CMD_TEST_BOOTLOADER);
				}
			
				// comando con i dati
				else if (mLineBuffer[0] == CMD_PROG_BOOTLOADER) {

					// dati, resetta contatore timeout
					cnt = 0;

					// scrive eeprom, continua finch'e torna true
					int rit = updateLineFirmware(mLineBuffer + 1);

					// testa se richiedere un altra riga da scrivere
					if (rit == RESULt_CONTINUE) {
						rs232Send(CMD_PROG_BOOTLOADER);
					}
					else if (rit == RESULt_FINISHED) {
						rs232Send(CMD_END_BOOTLOADER);
					}
					else {
						// errore, lascia andare il richiedente in TO ...												
					}
				}
			
				// testa se richiesta reboot
				else if (mLineBuffer[0] == CMD_END_BOOTLOADER) {
					asm("jmp 0000");
				}
			}
		}
		
		// fine gestione rs232


		// gestione CA_BUS, macchina a stati di ricezione
		switch(mRecvStatus) {
				
			case STATUS_CAN_ACCEPTED :
				if (can_cmd(&mMsg) == CAN_CMD_ACCEPTED) {
					mRecvStatus = STATUS_CAN_COMPLETED;
				}
				break;

			case STATUS_CAN_COMPLETED :
		
				// attendi comando completato
				if (can_get_status(&mMsg) != CAN_STATUS_NOT_COMPLETED) {
			
					// testa id, contiene i dati di programmazione
					if (mMsg.id.std  == ID_CAN_PROG_BOOTLOADER) {

						// dati, resetta contatore timeout
						cnt = 0;

						// costruisce il buffer
						for (U8 i = 0; i < mMsg.dlc; i++) {
							
							// testase fine buffer
							if (mCanBuffer[i] == '\r') {
								
								// scrive eeprom, continua finch'e torna true
								int rit = updateLineFirmware(mLineBuffer);

								// testa se richiedere un altra riga da scrivere
								if (rit == RESULt_CONTINUE) {
									canSend(ID_VALUE_PROG_BOOTLOADER);	
								}
								else if (rit == RESULt_FINISHED) {
									canSend(ID_VALUE_END_BOOTLOADER);	
								}
								else {
									canSend(ID_VALUE_RESEND_BOOTLOADER);
								}

								// prossima riga
								mIndexBuffer = 0;								
							}
							else {
								mLineBuffer[mIndexBuffer++] = mCanBuffer[i]; 			
							}
						}
					}
					
					// testa fine caricamento
					else if (mMsg.id.std  == ID_CAN_END_BOOTLOADER) {
						asm("jmp 0000");
					}
					
					// dati letti, torna in attesa
					mMsg.pt_data = mCanBuffer;
					mMsg.dlc = sizeof(mCanBuffer);
					mMsg.cmd = CMD_RX_DATA;
					mMsg.ctrl.ide = 0;
					mRecvStatus = STATUS_CAN_ACCEPTED;
				}
				break;
		
			default :
				break;
		}
		
		// fine gestione CAN-BUS
		
	}
				
				
	// reboot
	asm("jmp 0000");
	return 0;
 }
